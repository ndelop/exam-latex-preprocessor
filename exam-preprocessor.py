# -*- coding: utf-8 -*-
import pandas as pd
import re
import io
import subprocess
import logging
import argparse
from os import path
import sys
import tempfile

logging.basicConfig(format='%(levelname)s: %(message)s')
COMMAND_REGEX = re.compile(r"@(\w*)@")
COMMAND_REGEX_GROUP = 1


def get_students_df_from_file(filename):
    students_df = pd.read_excel(filename, dtype=object)
    return students_df


def read_tex_template_from_file(filename):
    with io.open(filename, mode='r', encoding='utf-8') as tex_template_file:
        tex_as_string = tex_template_file.read()
    return tex_as_string


def substitute_variables(tex_as_string, single_student_series):
    def replacement_func(match_obj):
        name_in_tex = match_obj.group(COMMAND_REGEX_GROUP)
        val = single_student_series[name_in_tex]
        if pd.isna(val):
            logging.warning(" ΑΕΜ {}: value {} missing".format(single_student_series['ΑΕΜ'], name_in_tex))
        return str(val)

    tex_with_variables = re.sub(COMMAND_REGEX, replacement_func, tex_as_string)
    return tex_with_variables


if __name__ == '__main__':
    # %% Argument parsing
    parser = argparse.ArgumentParser(description='Preprocessor for .tex exam files.')
    parser.add_argument('tex_file',
                        help='.tex exam template file')
    parser.add_argument('excel_file',
                        help='excel students and variables file')
    parser.add_argument('output_directory',
                        help='directory to store PDF files')

    parser.add_argument('-p', '--passes',
                        help='number of consecutive xelatex executions, increase for correct references, decrease for faster execution (default: %(default)s)',
                        type=int,
                        default=2,
                        metavar='n_passes')
    parser.add_argument('-e', '--early-stop',
                        help='stop after preparing %(metavar)s files',
                        type=int,
                        metavar='n_stop',
                        default=None)
    parser.add_argument('--dry-run',
                        help='generate .tex files only, no PDFs',
                        action='store_true')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='enable debug mode')

    args = parser.parse_args()
    # %%

    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.debug('---DEBUG MODE---')
    else:
        logging.getLogger().setLevel(logging.INFO)
        sys.tracebacklimit = 0

    logging.debug('Starting with: ' + str(args))

    exam_tex_name = path.abspath(args.tex_file)
    logging.info('LaTeX file: ' + exam_tex_name)

    students_excel_name = path.abspath(args.excel_file)
    logging.info('Excel name: ' + students_excel_name)

    xelatex_output_directory = path.abspath(args.output_directory)
    logging.info('Output directory: ' + xelatex_output_directory)

    xelatex_passes = args.passes
    logging.info('Number of xelatex passes: ' + str(xelatex_passes))

    early_stop = args.early_stop
    if early_stop is not None:
        logging.info('Early stop: going to stop after {} files'.format(early_stop))

    dry_run = args.dry_run
    if dry_run:
        logging.info('Dry run: no PDFs will be generated')

    # Create temporary directory for .tex, .aux, .log files
    temp_dir_obj = tempfile.TemporaryDirectory()
    logging.debug("Created temporary directory: " + temp_dir_obj.name)

    tex_file_dir = path.abspath(path.dirname(exam_tex_name))
    xelatex_temp_dir = temp_dir_obj.name

    # Read tex template
    tex_code = read_tex_template_from_file(exam_tex_name)
    logging.debug('Read .tex file.')

    # Read values from excel
    students = get_students_df_from_file(students_excel_name)
    logging.debug('Parsed excel file.')

    # Get user confirmation
    inp = input('Continue? ([y]/n)')
    if inp != 'y' and inp != 'Y' and inp != '':
        sys.exit()

    for index, current_student_row in students.iterrows():
        current_AEM = str(current_student_row['ΑΕΜ'])
        if early_stop is not None and index >= early_stop:
            break
        logging.info("ΑΕΜ={}: processing.".format(current_AEM))

        # substitute values for placeholders
        processed_tex_code = substitute_variables(tex_code, current_student_row)

        # create .tex for each student
        tex_file = path.abspath(path.join(temp_dir_obj.name, current_AEM + '.tex'))
        with io.open(tex_file, mode='w', encoding='utf-8') as f:
            f.write(processed_tex_code)
            logging.debug("ΑΕΜ={}: generated .tex file".format(current_AEM))

        # create .pdf for each student
        if not dry_run:
            for i in range(xelatex_passes):
                logging.debug('ΑΕΜ={}: xelatex pass {}'.format(current_AEM, i))
                subprocess.run(['xelatex', tex_file,
                                '-quiet',
                                '-include-directory=' + tex_file_dir,
                                # original directory, for figures and other share files
                                '-aux-directory=' + xelatex_temp_dir,  # directory for aux and log files
                                '-output-directory=' + xelatex_output_directory
                                ])
            logging.debug("ΑΕΜ={}: generated .pdf file.".format(current_AEM))

    # clear temporary directory
    logging.debug("Deleting temporary directory: " + temp_dir_obj.name)
    temp_dir_obj.cleanup()

    logging.info('Done. Bye!')
